variable "terraform_viewer_key" {
    description = "This is picked as TF_VAR gtlab variable"
}

variable "project_id" {
    default     = "internal-interview-candidates"
    description = "Project where resources needs to be created"
    }

variable "network_ready" {
    
}
