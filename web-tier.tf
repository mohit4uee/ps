data "template_file" "web_tier_startup_script" {
  template = file("${path.root}/web-tier-startup.sh.tpl")
  vars     = {}
}

resource "google_service_account" "web_server" {
  account_id   = "web-server"
  display_name = "Web Server Service Account"
}

data "google_compute_zones" "available" {
  region = "europe-west1"
}

module "mig1_web_tier_vm_instance_template" {
  source  = "terraform-google-modules/vm/google//modules/instance_template"
  version = "7.8.0"
  service_account = {
    email  = google_service_account.web_server.email,
    scopes = ["cloud-platform"]
  }

  startup_script = data.template_file.web_tier_startup_script.rendered
  network        = module.vpc.network_id
  subnetwork     = module.vpc.subnets_ids[0]
  region         = local.mig1_web_tier_region
  name_prefix    = local.mig1_web_tier_name
  labels         = local.labels
  tags           = [ "web-server" ]
}

module "mig1_web_tier" {
  source                    = "terraform-google-modules/vm/google//modules/mig"
  version                   = "7.8.0"
  autoscaling_enabled       = "true"
  min_replicas              = 1
  max_replicas              = 4
  instance_template         = module.mig1_web_tier_vm_instance_template.self_link
  project_id                = var.project_id
  region                    = local.mig1_web_tier_region
  distribution_policy_zones = data.google_compute_zones.available.names
  mig_name                  = local.mig1_web_tier_name
  network                   = module.vpc.network_id
  subnetwork                = module.vpc.subnets_ids[0]
  update_policy = [{
    type                           = "PROACTIVE"
    instance_redistribution_type   = "PROACTIVE"
    minimal_action                 = "REPLACE"
    most_disruptive_allowed_action = "REPLACE"
    min_ready_sec                  = 50
    replacement_method             = "RECREATE"
    max_surge_fixed                = 0
    max_unavailable_fixed          = 3
    max_surge_percent              = null
    max_unavailable_percent        = null


  }]
}
