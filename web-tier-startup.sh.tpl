#!/bin/bash

apt-get update
apt-get install -y apache2 libapache2-mod-php

sudo mv /var/www/html/index.html /var/www/html/index.html.old
sudo sed -i "s/Listen 80/Listen 8080/g" /etc/apache2/ports.conf

cat > /var/www/html/index.html <<'EOF'
<!doctype html>
<html>
<head>
<p>Welcome, this is your web-tier :)<p>
</head>

EOF

systemctl enable apache2
systemctl restart apache2

