#allow users from specific group to login as root on webserver to allow them to restart teh service.

resource "google_compute_project_metadata_item" "enable_os_login_project_level" {
  key   = "enable-oslogin"
  value = "TRUE"
}

resource "google_project_iam_member" "project_osadminlogin" {
    project= var.project_id
    role   = "roles/compute.osAdminLogin"
    member = "user:mohit4uee@gmail.com"
}

resource "google_service_account_iam_member" "project_osadminlogin_act_as_web_root" {
    service_account_id = google_service_account.web_server.id
    role               = "roles/iam.serviceAccountUser"
    member             = "user:mohit4uee@gmail.com"
}
