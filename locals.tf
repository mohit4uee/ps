locals {
  labels = {
    purpose = "web-tier",
  }
  # web tier ILB variables  
  ilb1_web_tier_region = "europe-west2"
  ilb1_web_tier_name   = "web-ilb"
  ilb1_web_tier_named_ports = [{
    name = "http"
    port = 8080
  }]


  ilb1_web_tier_health_check = {
    type                = "http"
    check_interval_sec  = 1
    healthy_threshold   = 4
    timeout_sec         = 1
    unhealthy_threshold = 5
    response            = ""
    proxy_header        = "NONE"
    port                = 8080
    port_name           = "health-check-port"
    request             = ""
    request_path        = "/"
    host                = google_compute_address.ip_address.address
    enable_log          = false
  }

  # web tier MIG variables 
  mig1_web_tier_region          = "europe-west1"
  mig1_web_tier_name            = "web-mig"

}
