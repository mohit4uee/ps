terraform {
  backend "http" {}
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "~> 4.26"
    }
    google-beta = {
      source = "hashicorp/google-beta"
      version = "~> 4.26"
    }
  }
}

provider "google" {
  project     = var.project_id
  credentials = var.terraform_viewer_key
}

provider "google-beta" {
  project     = var.project_id
  credentials = var.terraform_viewer_key
}
