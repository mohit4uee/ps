resource "google_compute_address" "ip_address" {
  name         = "web-server"
  subnetwork   = module.vpc.subnets_ids[0]
  address_type = "INTERNAL"
  region       = "europe-west1"
}

module "ilb1_web_tier" {
  source       = "GoogleCloudPlatform/lb-internal/google"
  count        = var.network_ready ? 1 : 0
  version      = "5.0.0"
  project      = var.project_id
  network      = module.vpc.network_name
  subnetwork   = module.vpc.subnets_names[0]
  region       = "europe-west1"
  name         = "group-ilb"
  ports        = [local.ilb1_web_tier_named_ports[0].port]
  source_tags  = []
  target_tags  = ["web-server"]
  ip_address   = google_compute_address.ip_address.address
  health_check = local.ilb1_web_tier_health_check

  backends = [
    {
      group       = module.mig1_web_tier.instance_group
      description = "Web Tier MIG"
      failover    = false
    },
  ]
}
